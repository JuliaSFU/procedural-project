
#ifndef __Program__
#define __Program__
#define _USE_MATH_DEFINES
#include <math.h>

namespace simple_shapes {

	// �������������
	struct rectangle {
		int x1, y1, x2, y2; // ������������� ���������� ������ �������� � ������� ������� �����
	};
	//����
	struct circle {
		int a, b, r; // ������������� ���������� ������ ����������, ������
	};

	// �����������
	struct triangle {
		int a1, b1, a2, b2, a3, b3; // ��� �����, �������� ������������� ���������� ������
	};

	// ���������, ���������� ��� ��������� ������
	struct shape {
		// �������� ����� ��� ������ �� �����
		enum color { RED, ORANGE, YELLOW, GREEN, BLUE, PURPLE };
		color c;
		float density;
		// �������� ������ ��� ������ �� �����
		enum key { RECTANGLE, CIRCLE, TRIANGLE };
		key k; 	   
		union { 
			rectangle r;
			circle t;
			triangle tr;
		};
	};

	// ��������� �� ������ ����������� ��������� ������
	struct node
	{
		shape* data;
		node* next;
		node(shape* data, node* next);
	};

	struct container
	{
		node* head;
	};

} // end simple_shapes namespace
#endif