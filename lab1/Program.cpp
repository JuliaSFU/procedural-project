#include "stdafx.h"
#include <fstream>
#include "Program.h"
#include <string>
#include <iostream>
#include <fstream>

using namespace std;

namespace simple_shapes {

	shape *In(ifstream &ifdt);
	void Out(shape &s, ofstream &ofst);
	void OutRect(shape &s, ofstream &ofst);
	void Sort(container &c);

	node::node(shape* data, node* next)
	{
		this->data = data;
		this->next = next;
	}

	// ������������� ����������
	void Init(container &c)
	{

		c.head = NULL;
	}
	// ������� ���������� �� ���������
	// (������������ ������)
	void Clear(container &c)
	{
		while (c.head != NULL)
		{
			node* forDelete = c.head;
			c.head = c.head->next;
			delete forDelete;
		}
	}
	// ���� ����������� ���������� �� ���������� ������
	void In(container &c, ifstream &ifst)
	{
		while (!ifst.eof()) {
			node* last = NULL;
			node* temp = new node(In(ifst), NULL);
			if (ifst.fail() || !ifst)
				 {
				c.head = NULL;
			    cout << "Input data is incorrect or the container is empty" << endl;
				break;
				}
			if (c.head == NULL)
				c.head = temp;
			else {
			last = c.head;
			while (last->next != NULL) last = last->next;
			last->next = temp;
		}
	}
	}
	
	// ����� ����������� ���������� � ��������� �����
	void Out(container &c, ofstream &ofst)
	{
		Sort(c);
		node* current = c.head;
		if (current == NULL)
			return;
		ofst << "Container contains elements:" << endl;
		while (current != NULL)
		{
			Out(*(current->data), ofst);
			current = current->next;
		}
	}

	//����� ����� ������
	void InColor(shape &s, int colorNumber)
	{
		switch (colorNumber)
		{
		case 1:
			s.c = shape::color::RED;
			break;
		case 2:
			s.c = shape::color::ORANGE;
			break;
		case 3:
			s.c = shape::color::YELLOW;
			break;
		case 4:
			s.c = shape::color::GREEN;
			break;
		case 5:
			s.c = shape::color::BLUE;
			break;
		case 6:
			s.c = shape::color::PURPLE;
			break;
		}
	}

	//����� ����� ������
	void OutColor(shape &s, ofstream &ofst)
	{
		switch (s.c)
		{
		case shape::color::RED:
			ofst << "RED" << endl;
			break;
		case shape::color::ORANGE:
			ofst << "ORANGE" << endl;
			break;
		case shape::color::YELLOW:
			ofst << "YELLOW" << endl;
			break;
		case shape::color::GREEN:
			ofst << "GREEN" << endl;
			break;
		case shape::color::BLUE:
			ofst << "BLUE" << endl;
			break;
		case shape::color::PURPLE:
			ofst << "PURPLE" << endl;
			break;
		default:
			ofst << "Color is not defined. Check the input data." << endl;
		}
	}


	// ���� ���������� �������������� �� �����
	void In(rectangle &r, ifstream &ifst)
	{
		ifst >> r.x1 >> r.y1 >> r.x2 >> r.y2;
	}
	// ����� ���������� �������������� � �����
	void Out(rectangle &r, ofstream &ofst) 
	{
		ofst << "It is Rectangle: x1 = " << r.x1
			<< ", y1 = " << r.y1
			<< ", x2 = " << r.x2
			<< ", y2 = " << r.y2 << ", color: ";
	}


	// ���� ���������� ����� �� ������
	void In(circle &t, ifstream &ifst)
	{
		ifst >> t.a >> t.b >> t.r;
	}
	// ����� ���������� ����� � �����
	void Out(circle &t, ofstream &ofst)
	{
		ofst << "It is Circle: a = "
			<< t.a << ", b = " << t.b
			<< ", r = " << t.r << ", color: ";
	}
	
	// ���������� ��������� ��������������
	float Perimeter(rectangle &r)
	{
		float p;
		if (r.x1 != r.x2 && r.y1 != r.y2)
			p = (abs(r.x2 - r.x1) + abs(r.y2 - r.y1)) * 2;
		else
			p = 0;
		return p;
	}	// ���������� ��������� �����
	float Perimeter(circle &t)
	{
		return t.r* M_PI * 2;
	}	float Perimeter(triangle &tr)
	{
		float a;
		float b;
		float c;
		a = sqrt((tr.a2 - tr.a1)*(tr.a2 - tr.a1) + (tr.b2 - tr.b1)*(tr.b2 - tr.b1));
		b = sqrt((tr.a3 - tr.a1)*(tr.a3 - tr.a1) + (tr.b3 - tr.b1)*(tr.b3 - tr.b1));
		c = sqrt((tr.a3 - tr.a2)*(tr.a3 - tr.a2) + (tr.b3 - tr.b2)*(tr.b3 - tr.b2));
		if (a == 0 || b == 0 || c == 0)
			 return 0;
		else
			return a + b + c;
	}	float Perimeter(shape &s)
	{
		switch (s.k)
		{
		case shape::key::RECTANGLE:
			return Perimeter(s.r);
		case shape::key::CIRCLE:
			return Perimeter(s.t);
		case shape::key::TRIANGLE:
			return Perimeter(s.tr);
		default:
			return -1;
		}
	}

	
	void In(triangle &tr, ifstream &ifst)
	{
		ifst >> tr.a1 >> tr.b1 >> tr.a2 >> tr.b2 >> tr.a3 >> tr.b3;
	}

	void Out(triangle &tr, ofstream &ofst)
	{
		ofst << "It is Triangle: x1 = " << tr.a1
			<< ", y1 = " << tr.b1
			<< ", x2 = " << tr.a2
			<< ", y2 = " << tr.b2
			<< ", x3 = " << tr.a3
			<< ", y3 = " << tr.b3 << ", color: ";
	}
	

	// ���� ���������� ���������� ������ �� �����
	shape* In(ifstream &ifst)
	{
		shape *sp;
		int k;
		int c;
		ifst >> k;
		switch (k) {
		case 1:
			sp = new shape;
			sp->k = shape::key::RECTANGLE;
			In(sp->r, ifst);
			ifst >> c;
			InColor(*sp, c);
			ifst >> sp->density;
			return sp;
		case 2:
			sp = new shape;
			sp->k = shape::key::CIRCLE;
			In(sp->t, ifst);
			ifst >> c;
			InColor(*sp, c);
			ifst >> sp->density;
			return sp;
		case 3:
			sp = new shape;
			sp->k = shape::key::TRIANGLE;
			In(sp->tr, ifst);
			ifst >> c;
			InColor(*sp, c);
			ifst >> sp->density;
			return sp;
		default:
			return 0;
		}
	}
	// ����� ���������� ������� ������ � �����
	void Out(shape &s, ofstream &ofst)
	{
		int m = 0;
		switch (s.k) {
		case shape::key::RECTANGLE:
			if (s.r.x1 != s.r.x2 && s.r.y1 != s.r.y2)
			{
				Out(s.r, ofst);
				OutColor(s, ofst);
				m = 1;
			}
			else
				ofst << "Attention! Check the input data. The rectangle's coordinates of the points must be different." << endl;
			break;
		case shape::key::CIRCLE:
			if (s.t.r > 0)
			{
				Out(s.t, ofst);
				OutColor(s, ofst);
				m = 1;
			}
			else
				ofst << "Attention! Check the input data. The radius of the circle must be greater than zero." << endl;
			break;
		case shape::key::TRIANGLE:
			if ((s.tr.a1 != s.tr.a2 && s.tr.b1 != s.tr.b2) &&
				(s.tr.a1 != s.tr.a3 && s.tr.b1 != s.tr.b3) &&
				(s.tr.a2 != s.tr.a3 && s.tr.b2 != s.tr.b3))
			{
				Out(s.tr, ofst);
				OutColor(s, ofst);
				m = 1;
			}
			else
				ofst << "Attention! Check the input data. The triangle's coordinates of the points must be different." << endl;
			break;
		default:
			ofst << "Incorrect figure!"
				<< endl;
		}
		if (m == 1)
		{
		if (s.density > 0)
			ofst << "density = " << s.density << endl;
		else
			ofst << "The density must be greater than zero. Check the input data." << endl;
		if (Perimeter(s) > 0)
			ofst << "perimeter = " << Perimeter(s) << endl;
		else
			ofst << "The perimeter must be greater than zero. Check the input data." << endl;
		}
	}
	void Swap(node* one, node* two)
	{
		shape* temp = one->data;
		one->data = two->data;
		two->data = temp;
	}
	// C�������� ������ ���� ����������� ��������
	bool Compare(shape *first, shape *second) {
		return Perimeter(*first) > Perimeter(*second);
	}	// ���������� ����������� ����������
	void Sort(container &c)
	{
		bool flag = true;
		while (flag)
		{
			flag = false;
			node* current = c.head;
			if (current == NULL)
				 return;
			while (current->next != NULL)
			{
				if ((Compare(current->data, current->next->data)) == true)
				{
					Swap(current, current->next);
					flag = true;
				}
				current = current->next;
			}
		}
	}

	void OutRect(shape &s, ofstream &ofst)
	{
		switch (s.k) {
		case shape::key::RECTANGLE:
			if (s.r.x1 != s.r.x2 && s.r.y1 != s.r.y2)
			{
			Out(s.r, ofst);
			OutColor(s, ofst);
			ofst << "density = " << s.density << endl;
			ofst << "perimeter = " << Perimeter(s) << endl;
			}
			else
				ofst << "Attention! Check the input data. The coordinates of the points must be different." << endl;
			break;
		case shape::key::CIRCLE:
			ofst << "";
			break;
		case shape::key::TRIANGLE:
			ofst << "";
			break;
		default:
			ofst << "Incorrect figure!"
				<< endl;
		}
	}

	void InCont(container &c, shape *sp)
		 {
		node* last = NULL;
		node* temp = new node(sp, NULL);
		if (c.head == NULL)
			c.head = temp;
		else {
			last = c.head;
			while (last->next != NULL) last = last->next;
			last->next = temp;
			
		}
		}
	
		shape* OutCont(container &c, int i)
		 {
		node *temp = c.head;
		int j = 0;
		while (temp != NULL)
			 {
			if (j == i)
				return (temp->data);
			j++;
			temp = temp->next;
			}
		}

			string StringForAssert(rectangle &r)
			 {
			string s = to_string(r.x1) + " " + to_string(r.y1) + " " + to_string(r.x2) + " "
				 +to_string(r.y2);
			return s;
			}
		
			string StringForAssert(circle &t)
			 {
			string s = to_string(t.a) + " " + to_string(t.b) + " " + to_string(t.r);
			return s;
			}
		
			string StringForAssert(triangle &tr)
			 {
			string s = to_string(tr.a1) + " " + to_string(tr.b1) + " " + to_string(tr.a2) + " " + 
				to_string(tr.b2) +" " + to_string(tr.a3) + " " + to_string(tr.b3);
			return s;
			}

			string StringForAssert(shape &s)
			{
				string str;
				switch (s.k)
				{
				case shape::key::RECTANGLE:
					str = StringForAssert(s.r);
				case shape::key::CIRCLE:
					str = StringForAssert(s.t);
				case shape::key::TRIANGLE:
					str = StringForAssert(s.tr);
				}
				str = str + " " + to_string(s.c)+ " " +to_string(s.density);
				return str;
			}

} // end simple_shapes namespace