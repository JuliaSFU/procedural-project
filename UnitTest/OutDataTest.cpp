#include "stdafx.h"
#include "CppUnitTest.h"
#include "Program.h"
#include <iostream>
#include <fstream>

using namespace std;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;
namespace simple_shapes {
	// ��������� ��������� ������� �������
	void Init(container&c);
	void InCont(container &c, shape *sp);
	void InColor(shape &s, int colorNumber);
	void Out(container &c, ofstream &ofst);
	void OutRect(shape &s, ofstream &ofst);
}
using namespace simple_shapes;

namespace OutDataTest
{
	TEST_CLASS(OutDataTest)
	{
	public:

		TEST_METHOD(OutEmptyCont)
			 {
			ofstream ofst("EmptyOutTest.txt");
			ofstream ifst("EmptyInTest.txt");
			container expected;
			Init(expected);
			Out(expected, ofst);
			
			ifstream ofs("EmptyOutTest.txt");
			ifstream ifs("EmptyInTest.txt");
		    string s;
			string exp;
			while (!ifs.eof())
				 {
				getline(ifs, exp);
				getline(ofs, s);
				Assert::AreEqual(exp, s);
				}
			}
		
		TEST_METHOD(OutFullCont)
		{
			ofstream ofst("OutTest.txt");
			ofstream ifst("InTest.txt");
			container c;
			Init(c);

			shape *tr1 = new shape;
			tr1->k = shape::key::TRIANGLE;
			tr1->tr.a1 = 0;
			tr1->tr.b1 = 0;
			tr1->tr.a2 = 1;
			tr1->tr.b2 = 1;
			tr1->tr.a3 = 2;
			tr1->tr.b3 = 2;
			InColor(*tr1, 3);
			tr1->density = 6.0;
			shape *r1 = new shape;
			r1->k = shape::key::RECTANGLE;
			r1->r.x1 = 0;
			r1->r.y1 = 0;
			r1->r.x2 = 2;
			r1->r.y2 = 2;
			InColor(*r1, 1);
			r1->density = 1.3;
			shape *�1 = new shape;
			�1->k = shape::key::CIRCLE;
			�1->t.a = 0;
			�1->t.b = 0;
			�1->t.r = 1;
			InColor(*�1, 2);
			�1->density = 5.1;

			InCont(c, tr1);
			InCont(c, �1);
			InCont(c, r1);
			Out(c, ofst);

			ifst << "Container contains elements:" << endl;
			ifst << "It is Triangle: x1 = 0, y1 = 0, x2 = 1, y2 = 1, x3 = 2, y3 = 2, color: YELLOW" << endl;
			ifst << "density = 6" << endl;
			ifst << "perimeter = 5.65685" << endl;
			ifst << "It is Circle: a = 0, b = 0, r = 1, color: ORANGE" << endl;
			ifst << "density = 5.1" << endl;
			ifst << "perimeter = 6.28319" << endl;
			ifst << "It is Rectangle: x1 = 0, y1 = 0, x2 = 2, y2 = 2, color: RED" << endl;
			ifst << "density = 1.3" << endl;
			ifst << "perimeter = 8" << endl;

			ifstream ofs("OutTest.txt");
			ifstream ifs("InTest.txt");
			string s;
			string exp;
			while (!ifs.eof())
				 {
				getline(ifs, exp);
				getline(ofs, s);
				Assert::AreEqual(exp, s);
				}
		}

		TEST_METHOD(OutRectTest)
		{
			ofstream ofst("OutRect.txt");
			ofstream ifst("InRect.txt");
			container c;
			Init(c);

			shape *tr1 = new shape;
			tr1->k = shape::key::TRIANGLE;
			tr1->tr.a1 = 0;
			tr1->tr.b1 = 0;
			tr1->tr.a2 = 1;
			tr1->tr.b2 = 1;
			tr1->tr.a3 = 2;
			tr1->tr.b3 = 2;
			InColor(*tr1, 3);
			tr1->density = 6.0;
			shape *r1 = new shape;
			r1->k = shape::key::RECTANGLE;
			r1->r.x1 = 0;
			r1->r.y1 = 0;
			r1->r.x2 = 2;
			r1->r.y2 = 2;
			InColor(*r1, 1);
			r1->density = 1.3;
			shape *�1 = new shape;
			�1->k = shape::key::CIRCLE;
			�1->t.a = 0;
			�1->t.b = 0;
			�1->t.r = 1;
			InColor(*�1, 2);
			�1->density = 5.1;

			OutRect(*tr1, ofst);
			OutRect(*�1, ofst);
			OutRect(*r1, ofst);

			ifst << "It is Rectangle: x1 = 0, y1 = 0, x2 = 2, y2 = 2, color: RED" << endl;
			ifst << "density = 1.3" << endl;
			ifst << "perimeter = 8" << endl;

			ifstream ofs("OutRect.txt");
			ifstream ifs("InRect.txt");
			string s;
			string exp;
			while (!ifs.eof())
			{
				getline(ifs, exp);
				getline(ofs, s);
				Assert::AreEqual(exp, s);
			}
		}
	};
}