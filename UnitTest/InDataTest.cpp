#include "stdafx.h"
#include "CppUnitTest.h"
#include "Program.h"
#include <iostream>
#include <fstream>

using namespace std;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;
namespace simple_shapes {
	// ��������� ��������� ������� �������
	void Init(container&c);
	void InCont(container &c, shape *sp);
	shape* OutCont(container &c, int i);
	string StringForAssert(shape &s);
	void In(container &c, ifstream &ifst);
	void InColor(shape &s, int colorNumber);
}
using namespace simple_shapes;

namespace InDataTest
{
	TEST_CLASS(InDataTest)
	{
	public:

		TEST_METHOD(InEmptyFile)
			{
			ifstream ifst("EmptyFile.txt");
			container c;
			Init(c);
			In(c, ifst);
			container expected;
			Init(expected);
			Assert::AreEqual(expected.head == NULL, c.head == NULL);
			}

		TEST_METHOD(InFullFile)
		{
			container c;
			Init(c);
			ifstream ifst("FullFile.txt");
			In(c, ifst);

			shape *tr1 = new shape;
			tr1->k = shape::key::TRIANGLE;
			tr1->tr.a1 = 0;
			tr1->tr.b1 = 0;
			tr1->tr.a2 = 1;
			tr1->tr.b2 = 1;
			tr1->tr.a3 = 2;
			tr1->tr.b3 = 2;
			InColor(*tr1, 3);
			tr1->density = 6.0;
			shape *r1 = new shape;
			r1->k = shape::key::RECTANGLE;
			r1->r.x1 = 0;
			r1->r.y1 = 0;
			r1->r.x2 = 2;
			r1->r.y2 = 2;
			InColor(*r1, 1);
			r1->density = 1.3;
			shape *�1 = new shape;
			�1->k = shape::key::CIRCLE;
			�1->t.a = 0;
			�1->t.b = 0;
			�1->t.r = 1;
			InColor(*�1, 2);
			�1->density = 5.1;
			shape *r2 = new shape;
			r2->k = shape::key::RECTANGLE;
			r2->r.x1 = 0;
			r2->r.y1 = 0;
			r2->r.x2 = 3;
			r2->r.y2 = 2;
			InColor(*r2, 3);
			r2->density = 3.3;

			container expected;
			Init(expected);
			InCont(expected, tr1);
			InCont(expected, r1);
			InCont(expected, r2);
			InCont(expected, �1);


			for (int i = 0; i < 4; i++)
			{
				shape* s = OutCont(c, i);
				shape* expect = OutCont(expected, i);
				Assert::AreEqual(StringForAssert(*expect), StringForAssert(*s));
			}
		}

		TEST_METHOD(InNonexistentFile)
			 {
			ifstream ifst("C://in.txt");
			container c;
			Init(c);
			In(c, ifst);
			container expected;
			Init(expected);
			Assert::AreEqual(expected.head == NULL, c.head == NULL);
			}

	};
}