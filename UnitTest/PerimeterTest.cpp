#include "stdafx.h"
#include "CppUnitTest.h"
#include "Program.h"
#include <iostream>
#include <fstream>
#define _USE_MATH_DEFINES
#include <math.h>

using namespace std;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace simple_shapes {
	// ��������� ��������� ������� �������
	float Perimeter(shape &s);
}
using namespace simple_shapes;

namespace PerimeterTest
{
	TEST_CLASS(PerimeterTest)
	{
	public:

		TEST_METHOD(PerimeterCircle)
		{
			float expected = 2 * M_PI;
			shape sp;
			sp.k = shape::key::CIRCLE;
			sp.t.a = 1;
			sp.t.b = 1;
			sp.t.r = 1;
			float p = Perimeter(sp);
			Assert::AreEqual(expected, p);
		}

		TEST_METHOD(PerimeterRectangle)
			{
			float expected = 8.0;
			shape sp;
			sp.k = shape::key::RECTANGLE;
			sp.r.x1 = 0;
			sp.r.y1 = 0;
			sp.r.x2 = 2;
			sp.r.y2 = 2;
			float p = Perimeter(sp);
			Assert::AreEqual(expected, p);
			}

		TEST_METHOD(PerimeterTriangle)
		{
			float expected = 12.0;
			shape sp;
			sp.k = shape::key::TRIANGLE;
			sp.tr.a1 = 0;
			sp.tr.b1 = 0;
			sp.tr.a2 = 0;
			sp.tr.b2 = 3;
			sp.tr.a3 = 4;
			sp.tr.b3 = 0;
			float p = Perimeter(sp);
			Assert::AreEqual(expected, p);
		}
	};
}