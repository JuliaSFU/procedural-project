#include "stdafx.h"
#include "CppUnitTest.h"
#include "Program.h"
#include <iostream>
#include <fstream>

using namespace std;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;
namespace simple_shapes {
	// ��������� ��������� ������� �������
	void Init(container&c);
	void InCont(container &c, shape *sp);
	shape* OutCont(container &c, int i);
	string StringForAssert(shape &s);
	void Sort(container &c);
	void InColor(shape &s, int colorNumber);
}
using namespace simple_shapes;

namespace SortTest
{
	TEST_CLASS(SortTest)
	{
	public:

		TEST_METHOD(TestSort)
		{
			container c;
			Init(c);

			shape *tr1 = new shape;
			tr1->k = shape::key::TRIANGLE;
			tr1->tr.a1 = 0;
			tr1->tr.b1 = 0;
			tr1->tr.a2 = 0;
			tr1->tr.b2 = 3;
			tr1->tr.a3 = 4;
			tr1->tr.b3 = 0;
			InColor(*tr1, 3);
			tr1->density = 6.0;
			shape *r1 = new shape;
			r1->k = shape::key::RECTANGLE;
			r1->r.x1 = 0;
			r1->r.y1 = 0;
			r1->r.x2 = 2;
			r1->r.y2 = 2;
			InColor(*r1, 1);
			r1->density = 1.3;
			shape *�1 = new shape;
			�1->k = shape::key::CIRCLE;
			�1->t.a = 0;
			�1->t.b = 0;
			�1->t.r = 1;
			InColor(*�1, 2);
			�1->density = 5.1;
			shape *r2 = new shape;
			r2->k = shape::key::RECTANGLE;
			r2->r.x1 = 0;
			r2->r.y1 = 0;
			r2->r.x2 = 3;
			r2->r.y2 = 2;
			InColor(*r2, 2);
			r2->density = 3;
			
			InCont(c, tr1);
			InCont(c, r1);
			InCont(c, r2);
			InCont(c, �1);
			
			container SortC;
			Init(SortC);
			InCont(SortC, �1);
			InCont(SortC, r1);
			InCont(SortC, r2);
			InCont(SortC, tr1);
			
			Sort(c);
			
				for (int i = 0; i < 4; i++)
				 {
				shape* s = OutCont(c, i);
				shape* expected = OutCont(SortC, i);
				Assert::AreEqual(StringForAssert(*expected), StringForAssert(*s));
				}
		}

	};
}